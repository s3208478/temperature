package nl.utwente.di.bookQuote;

import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

public class BookQuote extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private Quoter quoter;

    public void init() throws ServletException {
        quoter = new Quoter();
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String title = "Book Quote";

        String temp = request.getParameter("temperature");
        if (temp != null) {
            out.println("<!DOCTYPE HTML>\n" +
                                "<HTML>\n" +
                                "<HEAD><TITLE>" + title + "</TITLE>" +
                                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                                "</HEAD>\n" +
                                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                                "<H1>" + title + "</H1>\n" +
                                "  <P>Temp in C: " +
                                temp + "\n" +
                                "  <P>Temp in F: " +
                                quoter.getFahrenheint(temp) +
                                "</BODY></HTML>");
        }
    }
}